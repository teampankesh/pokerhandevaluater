#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <utility>
#include "declarations.h"
#include "adhocFunctions.h"

using namespace std;

bool isRoyalFlush( vector< card > hand) {
	return isStraightFlush(hand) and hand[0].value=='A';
}

bool isStraightFlush(vector< card > hand) {
	return isStraight(hand) and isFlush(hand);
}

bool isFourOfAKind(vector< card > hand) {
	map <char,int> CardCount = getSameCardCount(hand);
	for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
		if((it->second)==4) {
			return true;
		}
	}
	return false;
}

bool isFullHouse (vector<card> hand) {
	map <char,int> CardCount = getSameCardCount(hand);
	for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
		if((it->second)<2 or (it->second)>3) {
			return false;
		}
	}
	return true;
}

bool isFlush(vector < card > hand) {
	char Suit = hand[0].suit;

	for(unsigned int i=0; i< hand.size(); i++) {
		if(hand[i].suit!=Suit) {
			return false;
		}
	}
	return true;
}

bool isStraight( vector< card > hand) {
	map <char,int> CardCount = getSameCardCount(hand);
	for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
		if((it->second)>1) {
			return false;
		}
	}
	hand=handleLowAce(hand);
	for (unsigned int i=0; i<hand.size()-1; i++) {
		if((getCardValue(hand[i])-getCardValue(hand[i+1]))!=1) {
			return false;
		}
	}
	return true;
}

bool isThreeOfAKind(vector<card> hand) {
	map <char,int> CardCount = getSameCardCount(hand);
		for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
			if((it->second)==3) {
				return true;
			}
		}
	return false;
}

bool isTwoPair(vector<card> hand) {
    int twoPairCount = 0;
    map <char,int> CardCount = getSameCardCount(hand);
    for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
    	if((it->second)==2) {
    		twoPairCount++;
    	}
    }
    return (twoPairCount == 2);
}

bool isOnePair(vector<card> hand) {
    int onePairCount = 0;
    map <char,int> CardCount = getSameCardCount(hand);
    for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
    	if((it->second)==2) {
    		onePairCount++;
    	}
    }
    return (onePairCount==1);
}

bool isHighCard (vector<card> hand) {
    return true;
}
