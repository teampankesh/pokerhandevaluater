#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <utility>
#include "declarations.h"

using namespace std;

int getCardValue( card c) {
	map<char, int> cardValues;
	cardValues['1'] = 1;
	cardValues['2'] = 2;
	cardValues['3'] = 3;
	cardValues['4'] = 4;
	cardValues['5'] = 5;
	cardValues['6'] = 6;
	cardValues['7'] = 7;
	cardValues['8'] = 8;
	cardValues['9'] = 9;
	cardValues['T'] = 10;
	cardValues['J'] = 11;
	cardValues['Q'] = 12;
	cardValues['K'] = 13;
	cardValues['A'] = 14;
	return(cardValues[c.value]);
}

int winnerOfPokerHands( vector<card> vectorHand1, vector<card> vectorHand2 , int rankHand1, int rankHand2) {
	int result = 0;
	if (rankHand1 > rankHand2) {
		result = 1;
	}
	else if (rankHand1 < rankHand2) {
		result = -1;
	}
	else {
		result = compareHandsSuits( vectorHand1, vectorHand2);
	}
	return(result);
}

int assignRank ( vector<card> hand ) {
	int rank = 1000;
	if ( isRoyalFlush( hand ) ) {
		rank -= 0;
	}
	else if ( isStraightFlush( hand ) ) {
		rank -= 1;
	}
	else if (isFourOfAKind( hand ) ) {
		rank -= 2;
	}
	else if ( isFullHouse( hand ) ) {
		rank -= 3;
	}
	else if ( isFlush( hand ) ) {
		rank -= 4;
	}
	else if ( isStraight( hand ) ) {
		rank -= 5;
	}
	else if ( isThreeOfAKind( hand ) ) {
		rank -= 6;
	}
	else if ( isTwoPair( hand ) ) {
		rank -= 7;
	}
	else if ( isOnePair( hand ) ) {
		rank -= 8;
	}
	else if ( isHighCard( hand ) ) {
		rank -= 9;
	}
	return( rank );
}

int compareHandsSuits( vector<card> vectorHand1, vector<card> vectorHand2) {
	int worthHand1 = 0, worthHand2 = 0, handDiff = 0;
	for (unsigned int i=0; i<vectorHand1.size(); i++) {
		worthHand1 += getCardValue(vectorHand1[i]);
	}
	for (unsigned int i=0; i<vectorHand2.size(); i++) {
		worthHand2 += getCardValue(vectorHand2[i]);
	}
	handDiff = worthHand1 - worthHand2;
	if ( handDiff > 0 ) {
		return( 1 );
	}
	else if ( handDiff < 0 ) {
		return( -1 );
	}
	return ( 0 );
}
