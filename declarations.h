#include <vector>
#include <string>
#include <map>

using namespace std;

struct card {
	char value;
	char suit;
};

int getCardValue(card c);
int assignRank ( vector< card > hand);
int compareHandsSuits( vector<card> vectorHand1, vector<card> vectorHand2);
vector< card > makeCardVector (string hand);
vector< card > getSortedCardVector (vector< card > hand);
map< char,int > getSameCardCount (vector< card > hand);
vector< card > handleLowAce(vector< card > hand);
bool isRoyalFlush (vector< card > hand);
bool isStraightFlush (vector< card > hand);
bool isFourOfAKind (vector< card > hand);
bool isFlush (vector< card > hand);
bool isStraight (vector< card > hand);
bool isThreeOfAKind (vector<card>);
bool isOnePair (vector<card> hand);
bool isHighCard (vector<card> hand);
bool isFullHouse (vector<card> hand);
bool isTwoPair (vector<card> hand);
bool cmpFunction(const card& a, const card& b);
