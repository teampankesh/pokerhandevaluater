#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <utility>
#include "declarations.h"

using namespace std;

vector< card > makeCardVector(string hand){
    vector< card > vectorHand;
    card c;
    vector< card >:: iterator cards ;
    for (int i = 0; hand[i] !='\0';i = i+2){
        c.value = hand[i];
        c.suit = hand[i+1];
        vectorHand.insert(vectorHand.end(),c);
        cards++;
    }
    return vectorHand;
}

bool cmpFunction(const card& a, const card& b) {
	return getCardValue(a) > getCardValue(b);
}

vector< card > getSortedCardVector (vector< card > hand){
	sort(hand.begin(), hand.end(), cmpFunction);
    return hand;
}
