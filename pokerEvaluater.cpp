#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <utility>
#include "declarations.h"


using namespace std;

int main(int argc, char* argv[]) {
	struct card Card[5];
	unsigned int handSize=5;

	Card[0].value='K';
	Card[0].suit='S';
	Card[1].value='J';
	Card[1].suit='C';
	Card[2].value='A';
	Card[2].suit='S';
	Card[3].value='T';
	Card[3].suit='C';
	Card[4].value='A';
	Card[4].suit='C';

	vector<card> cardVector;
	vector<card> cardVector2;
	for (unsigned int i=0; i<handSize; i++) {
		cardVector.push_back(Card[i]);
	}
	cardVector=getSortedCardVector(cardVector);
	cout<<boolalpha<<isStraight(cardVector)<<endl;
	return 0;
}
