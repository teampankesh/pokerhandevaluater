#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <utility>
#include "declarations.h"

using namespace std;

int main(int argc, char* argv[]) {
	struct card Card[2];
	Card[0].value='T';
	Card[0].suit='S';
	Card[1].value='T';
	Card[1].suit='C';
	vector<card> cardVector;
	cardVector.push_back(Card[0]);
	cardVector.push_back(Card[1]);
	cout<<isStraight(cardVector)<<endl;
    return 0;
}

map< char, int > getSameCardCount(vector< card > hand) {
	map< char, int > sameCardCount;
	for (unsigned int i=0; i<hand.size();i++) {
		if(sameCardCount.find(hand[i].value) == sameCardCount.end()) {
			sameCardCount[hand[i].value]=1;
		}
		else {
			sameCardCount[hand[i].value]+=1;
		}
	}
	return sameCardCount;	
}

bool isRoyalFlush( vector< card > hand) {
	return isStraightFlush(hand) and hand[0].value=='A';
}   

bool isFlush(vector < card > hand) {
	char Suit = hand[0].suit;
	
	for(unsigned int i=0; i< hand.size(); i++) {
		if(hand[i].suit!=Suit) {
			return false;
		}
	}
	return true;
}

bool isStraightFlush(vector< card > hand) {
	return isStraight(hand) and isFlush(hand);
}

vector< card > handleLowAce(vector< card > hand) {
	vector< card> lowAceHand;
	struct card lowAce={'A','1'};
	for(unsigned int i=0; i<hand.size(); i++) {
		if (hand[i].value=='A' and (hand[1].value==5)) {
			lowAceHand.push_back(lowAce);
		} else {
			lowAceHand.push_back(hand[i]);
		}
	}
	return getSortedCardVector(lowAceHand);
}

bool isStraight( vector< card > hand) {
	map <char,int> CardCount = getSameCardCount(hand);
	for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
		if((it->second)>1) {
			return false;
		}
	}
	hand=handleLowAce(hand); 
	for (unsigned int i=0; i<hand.size()-1; i++) {
		if((getCardValue(hand[i])-getCardValue(hand[i+1]))!=1) {
			return false;
		}	
	}
	return true;
}

bool isFourOfAKind(vector< card > hand) {
	map <char,int> CardCount = getSameCardCount(hand);
	for (map<char,int>::iterator it=CardCount.begin(); it!=CardCount.end(); it++) {
		if((it->second)==4) {
			return true;
		}
	}
	return false;
}

